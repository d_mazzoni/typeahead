﻿var Main = (function () {
    var Typeahead = (function () {

        var Init = function () {

            var users = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: {
                    url: 'Data/generated_userdata.json',
                    filter: function (list) {
                        //If we control the API maybe we need filter in the server side the data to retrieve only active users and the Id and name fields
                        //If we do that this is not necessary
                        return $.map(list, function (val) {
                            if (val.isActive) return val.name;
                        });
                    }                    
                }
            });

            var setSource = function (q, sync, async) {
                if (q === '') {
                    sync(users.all());
                }
                else {
                    users.search(q, sync, async);
                }
            }

            $('.typeahead').typeahead(
                {
                    minLength: 0,
                },
                {
                    source: setSource,
                    limit: 20
                }
            );
        }      

        return {
            Init: Init           
        };
    })();

    return {
        Typeahead: Typeahead
    };
})();

$(document).ready(function () {
    Main.Typeahead.Init();
});